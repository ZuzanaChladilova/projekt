//
// Created by zajda on 07.05.2021.
//

#ifndef ZOO_PROJEKT_PREDMET_H
#define ZOO_PROJEKT_PREDMET_H


#include <string>

class Predmet {
    std::string m_nazev;
    int m_hodnota;
    int m_typ;
    bool m_jeLektvar;

public:
    Predmet(std::string nazev, int hodnota, int typ, bool jeLektvar);

    void setNazev(std::string nazev);

    void setHodnota(int hodnota);

    void setTyp(int typ);

    std::string getNazev();

    int getHodnota();

    int getTyp();

    void printInfo();
};


#endif //ZOO_PROJEKT_PREDMET_H
