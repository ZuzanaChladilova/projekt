//
// Created by zajda on 07.05.2021.
//

#include <iostream>
#include "Predmet.h"

Predmet::Predmet(std::string nazev, int hodnota, int typ, bool jeLektvar) {
    m_nazev = nazev;
    m_hodnota = hodnota;
    m_typ = typ;
    m_jeLektvar = jeLektvar;
}

void Predmet::setNazev(std::string nazev) {
    m_nazev = nazev;
}

void Predmet::setHodnota(int hodnota) {
    m_hodnota = hodnota;
}

void Predmet::setTyp(int typ) {
    m_typ = typ;
}

std::string Predmet::getNazev() {
    return m_nazev;
}

int Predmet::getHodnota() {
    return m_hodnota;
}

int Predmet::getTyp() {
    return m_typ;
}

void Predmet::printInfo() {
    std::cout << "   Nazev: " << m_nazev << std::endl;
    std::cout << "   Hodnota bonusu: " << m_hodnota << std::endl;
    std::cout << std::endl;
}
