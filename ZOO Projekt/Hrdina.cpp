//
// Created by zajda on 12.05.2021.
//

#include "Hrdina.h"

void Hrdina::branSe() {

}

void Hrdina::setAktualniLokace(Lokace *lokace) {
    m_aktualniLokace = lokace;
}

void Hrdina::zahodPredmet(int pozice) {
    m_predmety.erase(m_predmety.begin() + pozice);
}

void Hrdina::seberPredmet(Predmet *predmet) {
    m_predmety.push_back(predmet);
}

Lokace *Hrdina::getAktualniLokace() {
    return m_aktualniLokace;
}

// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
