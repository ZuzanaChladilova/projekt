//
// Created by zajda on 07.05.2021.
// Updated by Majkl on 07.05.2021.
//

#ifndef ZOO_PROJEKT_POSTAVA_H
#define ZOO_PROJEKT_POSTAVA_H


#include <string>
#include <vector>
#include "Predmet.h"
//#include "Lokace.h"

class Postava {
protected:
    int m_sila;
    int m_zdravi;
    int m_hbitost;
    int m_inteligence;
    std::vector<Predmet *> m_predmety;

public:
    std::string m_jmeno;

    Postava();

    std::string getJmeno();

    void setZdravi(int zdravi);

    void setSila(int sila);

    void setInteligence(int inteligence);

    void setHbitost(int hbitost);

    void setJmeno(std::string jmeno);

    void seberPredmet(Predmet *predmet);

    void pouzijPredmet(int pozice);

    void zahodPredmet(int pozice);

    int getUtok();

    int getObrana();

    //void zmenLokaci(Lokace* lokace);
    //void setAktualniLokace(Lokace* lokace);
    void printInfo();

    void zautoc();

    void branSe();

    void vypisPredmety();

    ~Postava();

};


#endif //ZOO_PROJEKT_POSTAVA_H
