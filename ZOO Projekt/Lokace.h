//
// Created by zajda on 06.05.2021.
//

#ifndef ZOO_PROJEKT_LOKACE_H
#define ZOO_PROJEKT_LOKACE_H


#include <vector>
#include <string>
#include "Postava.h"
#include "Predmet.h"

class Lokace {
    std::string m_nazev;
    std::string m_popis;
    Lokace *m_severniLokace;
    Lokace *m_jizniLokace;
    Lokace *m_zapadniLokace;
    Lokace *m_vychodniLokace;
    std::vector<Postava *> m_nepratele;
    std::vector<Predmet *> m_predmety;

public:
    Lokace(std::string nazev, std::string popis);

    ~Lokace();

    void setNazev(std::string nazev);

    void setPopis(std::string popis);

    void setSeverniLokace(Lokace *severniLokace);

    void setJizniLokace(Lokace *jizniLokace);

    void setZapadniLokace(Lokace *zapadniLokace);

    void setVychodniLokace(Lokace *vychodniLokace);

    Lokace *getSeverniLokace();

    Lokace *getJizniLokace();

    Lokace *getZapadniLokace();

    Lokace *getVychodniLokace();

    std::string getNazev();

    std::vector<Postava *> getNepratele();

    std::vector<Predmet *> getPredmety();

    void pridejNepritele(Postava *nepritel);

    void pridejPredmet(Predmet *predmet);

    void vyberAkci();

    void printInfo();

};


#endif //ZOO_PROJEKT_LOKACE_H
