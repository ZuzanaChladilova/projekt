//
// Created by zajda on 07.05.2021.
//

#ifndef ZOO_PROJEKT_LOKACEBUILDER_H
#define ZOO_PROJEKT_LOKACEBUILDER_H


#include "../Lokace.h"

class LokaceBuilder {
protected:
    Lokace *m_lokace;

public:
    LokaceBuilder();

    void vytvorLokaci();

    virtual void setOkolniLokace() = 0;

    virtual void setNazev() = 0;

    virtual void setPopis() = 0;

    virtual void setNepratele() = 0;

    virtual void setPredmety() = 0;

    Lokace *getLokace();
};


#endif //ZOO_PROJEKT_LOKACEBUILDER_H
