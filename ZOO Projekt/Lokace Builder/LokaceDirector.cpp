//
// Created by zajda on 07.05.2021.
//

#include "LokaceDirector.h"

LokaceDirector::LokaceDirector(LokaceBuilder *lokaceBuilder) {
    m_lokaceBuilder = lokaceBuilder;
}

void LokaceDirector::setLokaceBuilder(LokaceBuilder *lokaceBuilder) {
    m_lokaceBuilder = lokaceBuilder;
}

Lokace *LokaceDirector::sestavLokaci() {
    m_lokaceBuilder->vytvorLokaci();
    m_lokaceBuilder->setNazev();
    m_lokaceBuilder->setPopis();
    m_lokaceBuilder->setOkolniLokace();
    m_lokaceBuilder->setNepratele();
    m_lokaceBuilder->setPredmety();
    return m_lokaceBuilder->getLokace();
}
