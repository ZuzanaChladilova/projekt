//
// Created by zajda on 07.05.2021.
//

#include "LesBuilder.h"

void LesBuilder::setOkolniLokace() {
//    m_lokace->setSeverniLokace();
//    m_lokace->setJizniLokace();
//    m_lokace->setZapadniLokace();
//    m_lokace->setVychodniLokace();
}

void LesBuilder::setNazev() {
    m_lokace->setNazev("Les");
}

void LesBuilder::setPopis() {
    m_lokace->setPopis("Temny les s bandou nepratel, snadno se ztratis, tak davej pozor.");
}

void LesBuilder::setNepratele() {
//    m_lokace->pridejNepritele();
//    m_lokace->pridejNepritele();
//    m_lokace->pridejNepritele();
}

// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void LesBuilder::setPredmety() {
    m_lokace->pridejPredmet(new Predmet("Lektvar sily", 10, 2, true));
    m_lokace->pridejPredmet(new Predmet("Inteligentni plast", 15, 3, false));
    m_lokace->pridejPredmet(new Predmet("Lektvar hbitosti", 10, 1, true));
    m_lokace->pridejPredmet(new Predmet("Ochranna helma", 8, 0, false));
}
