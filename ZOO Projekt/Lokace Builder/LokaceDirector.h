//
// Created by zajda on 07.05.2021.
//

#ifndef ZOO_PROJEKT_LOKACEDIRECTOR_H
#define ZOO_PROJEKT_LOKACEDIRECTOR_H


#include "LokaceBuilder.h"

class LokaceDirector {
    LokaceBuilder *m_lokaceBuilder;

public:
    LokaceDirector(LokaceBuilder *lokaceBuilder);

    void setLokaceBuilder(LokaceBuilder *lokaceBuilder);

    Lokace *sestavLokaci();
};


#endif //ZOO_PROJEKT_LOKACEDIRECTOR_H
