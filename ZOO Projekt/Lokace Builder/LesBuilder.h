//
// Created by zajda on 07.05.2021.
//

#ifndef ZOO_PROJEKT_LESBUILDER_H
#define ZOO_PROJEKT_LESBUILDER_H


#include "LokaceBuilder.h"

class LesBuilder : public LokaceBuilder {
public:
    void setOkolniLokace() override;

    void setNazev() override;

    void setPopis() override;

    void setNepratele() override;

    void setPredmety() override;
};


#endif //ZOO_PROJEKT_LESBUILDER_H
