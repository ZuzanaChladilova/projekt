//
// Created by zajda on 07.05.2021.
//

#include "PoleBuilder.h"
#include "../Postava Builder/PostavaDirector.h"
#include "../Postava Builder/BanditaBuilder.h"
#include "LokaceDirector.h"
#include "MestoBuilder.h"
#include "../Postava Builder/NastvanySousedBuilder.h"

void PoleBuilder::setOkolniLokace() { // todo?
    m_lokace->setSeverniLokace(reinterpret_cast<Lokace *>(new LokaceDirector(new MestoBuilder)));
    m_lokace->setJizniLokace(reinterpret_cast<Lokace *>(new LokaceDirector(new MestoBuilder)));
    m_lokace->setZapadniLokace(reinterpret_cast<Lokace *>(new LokaceDirector(new MestoBuilder)));
    m_lokace->setVychodniLokace(reinterpret_cast<Lokace *>(new LokaceDirector(new MestoBuilder)));
}

void PoleBuilder::setNazev() {
    m_lokace->setNazev("Pole");
}

void PoleBuilder::setPopis() {
    m_lokace->setPopis("Rozsahle pole s vysokou kukurici, nejspis zde bude mnoho napomocnych predmetu, pri hledani ale muzes narazit na nepratele.");
}

void PoleBuilder::setNepratele() { // todo?
   /* m_lokace->pridejNepritele(reinterpret_cast<Postava *>(new PostavaDirector(new BanditaBuilder)));
    m_lokace->pridejNepritele(reinterpret_cast<Postava *>(new PostavaDirector(new BanditaBuilder)));
    m_lokace->pridejNepritele(reinterpret_cast<Postava *>(new PostavaDirector(new NastvanySousedBuilder)));
    m_lokace->pridejNepritele(reinterpret_cast<Postava *>(new PostavaDirector(new NastvanySousedBuilder)));
    */
}

// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void PoleBuilder::setPredmety() {
    m_lokace->pridejPredmet(new Predmet("Lektvar sily", 1, 2, true));
    m_lokace->pridejPredmet(new Predmet("Inteligentni rukavice", 5, 3, false));
    m_lokace->pridejPredmet(new Predmet("Lektvar hbitosti", 15, 1, true));
    m_lokace->pridejPredmet(new Predmet("Lektvar zdravi", 20, 0, true));
}
