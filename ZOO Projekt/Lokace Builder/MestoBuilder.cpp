//
// Created by zajda on 07.05.2021.
//

#include "MestoBuilder.h"

void MestoBuilder::setOkolniLokace() {
//  todo  m_lokace->setSeverniLokace();
//    m_lokace->setJizniLokace();
//    m_lokace->setZapadniLokace();
//    m_lokace->setVychodniLokace();
}

void MestoBuilder::setNazev() {
    m_lokace->setNazev("Mesto");
}

void MestoBuilder::setPopis() {
    m_lokace->setPopis("Mesto se spatnou povesti, udelej co musis a pak rychle pryc.");
}

void MestoBuilder::setNepratele() {
// todo   m_lokace->pridejNepritele();
//    m_lokace->pridejNepritele();
}

// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void MestoBuilder::setPredmety() {
    m_lokace->pridejPredmet(new Predmet("Lektvar sily", 13, 2, true));
    m_lokace->pridejPredmet(new Predmet("Inteligentni ponozky", 11, 3, false));
    m_lokace->pridejPredmet(new Predmet("Pasek hbitosti", 3, 1, false));
    m_lokace->pridejPredmet(new Predmet("Ochranna maska", 4, 0, false));
}
