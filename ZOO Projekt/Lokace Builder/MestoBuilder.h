//
// Created by zajda on 07.05.2021.
//

#ifndef ZOO_PROJEKT_MESTOBUILDER_H
#define ZOO_PROJEKT_MESTOBUILDER_H


#include "LokaceBuilder.h"

class MestoBuilder : public LokaceBuilder {
public:
    void setOkolniLokace() override;

    void setNazev() override;

    void setPopis() override;

    void setNepratele() override;

    void setPredmety() override;
};


#endif //ZOO_PROJEKT_MESTOBUILDER_H
