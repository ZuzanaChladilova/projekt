//
// Created by Majkl on 11. 5. 2021.
//

#ifndef ZOO_PROJEKT_HRA_H
#define ZOO_PROJEKT_HRA_H


#include "Lokace.h"
#include "Hrdina.h"

class Hra {
    Hrdina *m_hrdina;

//    Lokace *mesto;
//    Lokace *les;
//    Lokace *pole;
//    Lokace *vesnice;
//    Lokace *cesta;
//    Lokace *hrbitov;
//    Lokace *louka;

    Lokace *m_aktualniLokace;
public:
    //void setOkolniLokace(Lokace *sever, Lokace *jih, Lokace *zapad, Lokace *vychod);

    bool m_konec;

    Hra();

    void spustitHru();

    void vyberAkci();

private:
    void jdiJinam();

    //void seberPredmet();


};


#endif //ZOO_PROJEKT_HRA_H
