//
// Created by zajda on 06.05.2021.
//

#include <iostream>
#include "Lokace.h"

Lokace::Lokace(std::string nazev, std::string popis) {
    m_nazev = nazev;
    m_popis = popis;
    m_severniLokace = nullptr;
    m_jizniLokace = nullptr;
    m_zapadniLokace = nullptr;
    m_vychodniLokace = nullptr;
    m_nepratele = {};
    m_predmety = {};
}

Lokace::~Lokace() {

}

void Lokace::setNazev(std::string nazev) {
    m_nazev = nazev;
}

void Lokace::setPopis(std::string popis) {
    m_nazev = popis;
}

void Lokace::setSeverniLokace(Lokace *severniLokace) {
    m_severniLokace = severniLokace;
}

void Lokace::setJizniLokace(Lokace *jizniLokace) {
    m_jizniLokace = jizniLokace;
}

void Lokace::setZapadniLokace(Lokace *zapadniLokace) {
    m_zapadniLokace = zapadniLokace;
}

void Lokace::setVychodniLokace(Lokace *vychodniLokace) {
    m_vychodniLokace = vychodniLokace;
}

Lokace *Lokace::getSeverniLokace() {
    return m_severniLokace;
}

Lokace *Lokace::getJizniLokace() {
    return m_jizniLokace;
}

Lokace *Lokace::getZapadniLokace() {
    return m_zapadniLokace;
}

Lokace *Lokace::getVychodniLokace() {
    return m_vychodniLokace;
}

std::string Lokace::getNazev() {
    return m_nazev;
}

std::vector<Postava *> Lokace::getNepratele() {
    return m_nepratele;
}

std::vector<Predmet *> Lokace::getPredmety() {
    return m_predmety;
}

void Lokace::pridejNepritele(Postava *nepritel) {
    m_nepratele.push_back(nepritel);
}

void Lokace::pridejPredmet(Predmet *predmet) {
    m_predmety.push_back(predmet);
}

//void Lokace::vyberAkci() {
//    char rozhodnuti;
//    char rozhodnuti2;
//    std::cout << "Co chces delat?" << std::endl;
//    std::cout << "Mas tyto motnosti: " << std::endl;
//    std::cout << "Jit [j]inam, sebrat [p]redmet, [b]ojovat." << std::endl;
//    std::cin.get(rozhodnuti);
//    if (rozhodnuti == 'j') {
//        std::cout << "Kam chces jit?" << std::endl;
//        std::cout << "Na [s]everu je " << m_severniLokace->getNazev() << std::endl;
//        std::cout << "Na [j]ihu je " << m_jizniLokace->getNazev() << std::endl;
//        std::cout << "Na [z]apade je " << m_zapadniLokace->getNazev() << std::endl;
//        std::cout << "Na [v]ychode je " << m_vychodniLokace->getNazev() << std::endl;
//        std::cin >> rozhodnuti2;
//        if (rozhodnuti2 == 's') {
//            //todo Hrdina->setAktualniLokace(m_severniLokace);
//        } else if (rozhodnuti2 == 'j') {
//            //todo Hrdina->setAktualniLokace(m_jizniLokace);
//        } else if (rozhodnuti2 == 'z') {
//            //todo Hrdina->setAktualniLokace(m_zapadniLokace);
//        } else if (rozhodnuti2 == 'v') {
//            //todo Hrdina->setAktualniLokace(m_vychodniLokace);
//        } else {
//            std::cout << "Spatny vstup." << std::endl;
//        }
//    } else if (rozhodnuti == 'p') {
//        std::cout << "Ktery predmet chces?" << std::endl;
//        for (int i = 0; i < m_predmety.size(); ++i) {
//            std::cout << "[" << i << "] " << std::endl;
//            m_predmety.at(i)->printInfo();
//        }
//        std::cin.get(rozhodnuti2);
//        // todo Hrdina->pridejPredmet(m_predmety.at(std::stoi(&rozhodnuti2)));
//        m_predmety.erase(m_predmety.begin() + std::stoi(&rozhodnuti2));
//    } else if (rozhodnuti == 'b') {
//        std::cout << "Vyber nepritele." << std::endl;
//        for (int i = 0; i < m_nepratele.size(); ++i) {
//            std::cout <<"[" << i<< "] " << std::endl;
//            m_nepratele.at(i)->printInfo();
//        }
//        std::cin.get(rozhodnuti2);
//
//        // todo while (m_nepratele.at(std::stoi(&rozhodnuti2))->getZdravi() > 0 and Hrdina->getZdravi() > 0) {
//        //    m_nepratele.at(std::stoi(&rozhodnuti2))->zautoc();
//        //    Hrndina->branSe();
//        //    Hrndina->Zautoc();
//        //    m_nepratele.at(std::stoi(&rozhodnuti2))->branSe();
//
//    }
//}

void Lokace::printInfo() {
    std::cout << std::endl;
    std::cout << "Nazev: " << m_nazev << std::endl;
    std::cout << "Popis: " << m_popis << std::endl;
    std::cout << "Nachazeji se zde tito nepratele: " << std::endl;
    std::cout << std::endl;
    for (int i = 0; i < m_nepratele.size(); ++i) {
        m_nepratele.at(i)->printInfo();
    }
    std::cout << "Nachazeji se zde tyto predmety: " << std::endl;
    std::cout << std::endl;
    for (int i = 0; i < m_predmety.size(); ++i) {
        m_predmety.at(i)->printInfo();
    }
}


