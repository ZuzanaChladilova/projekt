//
// Created by Majkl on 7. 5. 2021.
//

#include "PostavaBuilder.h"

PostavaBuilder::PostavaBuilder() {
    m_postava = nullptr;
}

void PostavaBuilder::vytvorPostavu() {
    m_postava = new Postava();
}

Postava *PostavaBuilder::getPostava() {
    return m_postava;
}
