//
// Created by Majkl on 7. 5. 2021.
//

#include "BanditaBuilder.h"

void BanditaBuilder::setVlastnosti() {
    m_postava->setJmeno("Bandita");
    m_postava->setZdravi(500);
    m_postava->setSila(15);
    m_postava->setHbitost(7);
    m_postava->setInteligence(11);

}
// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void BanditaBuilder::setPredmety() {
    m_postava->seberPredmet(new Predmet("Lektvar zdravi", 25, 0, true));
    m_postava->seberPredmet(new Predmet("Dyka", 75, 100, false));
    m_postava->seberPredmet(new Predmet("Kozena zbroj", 50, 200, false));
    m_postava->seberPredmet(new Predmet("Pivo", 2, 1, true));


}
