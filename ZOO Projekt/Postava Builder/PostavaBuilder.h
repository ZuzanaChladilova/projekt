//
// Created by Majkl on 7. 5. 2021.
//

#ifndef ZOO_PROJEKT_POSTAVABUILDER_H
#define ZOO_PROJEKT_POSTAVABUILDER_H


#include "../Postava.h"
#include "../Hrdina.h"

class PostavaBuilder {
protected:
    Postava *m_postava;
 //   Hrdina *m_hrdina;

public:
    PostavaBuilder();

    void vytvorPostavu();

    virtual void setVlastnosti() = 0;

    virtual void setPredmety() = 0;

    Postava *getPostava();

};


#endif //ZOO_PROJEKT_POSTAVABUILDER_H
