//
// Created by Majkl on 7. 5. 2021.
//

#include "NastvanySousedBuilder.h"

void NastvanySousedBuilder::setVlastnosti() {
    m_postava->setJmeno("Soused");
    m_postava->setZdravi(250);
    m_postava->setSila(10);
    m_postava->setHbitost(3);
    m_postava->setInteligence(7);

}
// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void NastvanySousedBuilder::setPredmety() {
    m_postava->seberPredmet(new Predmet("Lektvar zdravi", 25, 0, true));
    m_postava->seberPredmet(new Predmet("Koste", 25, 100, false));
    m_postava->seberPredmet(new Predmet("Koralka", -1, 1, true));


}