//
// Created by Majkl on 9. 5. 2021.
//

#include "HrdinaBuilder.h"


void HrdinaBuilder::setVlastnosti() {
    m_postava->setJmeno("ANONYM");
    m_postava->setZdravi(400);
    m_postava->setSila(12);
    m_postava->setHbitost(5);
    m_postava->setInteligence(10);

}
// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

void HrdinaBuilder::setPredmety() {
    m_postava->seberPredmet(new Predmet("Voda", 5, 0, true));

}