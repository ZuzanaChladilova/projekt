//
// Created by Majkl on 7. 5. 2021.
//

#ifndef ZOO_PROJEKT_BANDITABUILDER_H
#define ZOO_PROJEKT_BANDITABUILDER_H


#include "PostavaBuilder.h"

class BanditaBuilder : public PostavaBuilder {
    //std::string m_jmeno;
    void setVlastnosti() override;

    void setPredmety() override;

};


#endif //ZOO_PROJEKT_BANDITABUILDER_H
