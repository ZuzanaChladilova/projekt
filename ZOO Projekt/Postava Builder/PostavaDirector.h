//
// Created by Majkl on 7. 5. 2021.
//

#ifndef ZOO_PROJEKT_POSTAVADIRECTOR_H
#define ZOO_PROJEKT_POSTAVADIRECTOR_H


#include "PostavaBuilder.h"

class PostavaDirector {
    PostavaBuilder *m_builder;

public:
    PostavaDirector(PostavaBuilder *builder);

    void setPostavaBuilder(PostavaBuilder *builder);

    Postava *sestavPostavu();
};


#endif //ZOO_PROJEKT_POSTAVADIRECTOR_H
