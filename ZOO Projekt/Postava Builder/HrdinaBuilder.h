//
// Created by Majkl on 9. 5. 2021.
//

#ifndef ZOO_PROJEKT_HRDINABUILDER_H
#define ZOO_PROJEKT_HRDINABUILDER_H


#include "PostavaBuilder.h"

class HrdinaBuilder : public PostavaBuilder {
    void setVlastnosti() override;

    void setPredmety() override;

};


#endif //ZOO_PROJEKT_HRDINABUILDER_H
