//
// Created by Majkl on 7. 5. 2021.
//

#include "PostavaDirector.h"

PostavaDirector::PostavaDirector(PostavaBuilder *builder) {
    m_builder = builder;

}

void PostavaDirector::setPostavaBuilder(PostavaBuilder *builder) {
    m_builder = builder;

}

Postava *PostavaDirector::sestavPostavu() {
    m_builder->vytvorPostavu();
    m_builder->setVlastnosti();
    m_builder->setPredmety();

    return m_builder->getPostava();
}
