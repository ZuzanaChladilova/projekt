//
// Created by Majkl on 7. 5. 2021.
//

#ifndef ZOO_PROJEKT_NASTVANYSOUSEDBUILDER_H
#define ZOO_PROJEKT_NASTVANYSOUSEDBUILDER_H


#include "PostavaBuilder.h"

class NastvanySousedBuilder : public PostavaBuilder {
    void setVlastnosti() override;

    void setPredmety() override;

};


#endif //ZOO_PROJEKT_NASTVANYSOUSEDBUILDER_H
