//
// Created by zajda on 12.05.2021.
//

#ifndef ZOO_PROJEKT_HRDINA_H
#define ZOO_PROJEKT_HRDINA_H


#include "Lokace.h"

class Hrdina : public Postava {
    Lokace* m_aktualniLokace;

public:
    void branSe();

    void setAktualniLokace(Lokace *lokace);

    Lokace* getAktualniLokace();

    void zahodPredmet(int pozice);

    void seberPredmet(Predmet *predmet);

    void pouzijPredmet(int pozice);
};


#endif //ZOO_PROJEKT_HRDINA_H
