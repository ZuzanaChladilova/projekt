//
// Created by Majkl on 11. 5. 2021.
//

#include <iostream>
#include <conio.h>
#include "Hra.h"
#include "Postava Builder/PostavaDirector.h"
#include "Postava Builder/BanditaBuilder.h"
#include "Postava Builder/HrdinaBuilder.h"
#include "Lokace Builder/LokaceDirector.h"
#include "Lokace Builder/LesBuilder.h"
#include "Lokace Builder/PoleBuilder.h"
#include "Lokace Builder/MestoBuilder.h"
#include "Postava Builder/NastvanySousedBuilder.h"
#include "Hrdina.h"

Hra::Hra() {
    Lokace *mesto = new Lokace("Velke mesto", "neni znam");
    Lokace *les = new Lokace("Temny les", "neni znam");
    Lokace *pole = new Lokace("Rozlehle pole", "neni znam");
    Lokace *vesnice = new Lokace("Mala vesnice", "neni znam");
    Lokace *cesta = new Lokace("Dlouha cesta", "neni znam");
    Lokace *hrbitov = new Lokace("Tajemny hrbitov", "neni znam");
    Lokace *louka = new Lokace("Kvetinova louka", "neni znam");
    m_konec = false;

    mesto->setSeverniLokace(les);
    mesto->setJizniLokace(pole);
    mesto->setVychodniLokace(vesnice);
    mesto->setZapadniLokace(cesta);
// typ 0 patri ke zdravi
// typ 1 patri k hbitosti
// typ 2 parti k sile
// typ 3 patri k inteligenci
// typ 100 patri k utoku
// typ 200 patri k obrane

    mesto->pridejPredmet(new Predmet("Lektvar sily", 15, 2, true));
    mesto->pridejPredmet(new Predmet("Inteligentni ponozky", 11, 3, false));

    mesto->pridejNepritele(new Postava);

    les->setSeverniLokace(hrbitov);
    les->setJizniLokace(mesto);
    les->setZapadniLokace(louka);

    les->pridejPredmet(new Predmet("Lektvar zdravi", 15, 0, true));
    les->pridejPredmet(new Predmet("Hbite ponozky", 11, 1, false));

    les->pridejNepritele(new Postava);

    pole->setSeverniLokace(mesto);

    pole->pridejPredmet(new Predmet("Lektvar zdravi", 10, 0, true));
    pole->pridejNepritele(new Postava);

    vesnice->setVychodniLokace(mesto);

    vesnice->pridejPredmet(new Predmet("Lektvar zdravi", 5, 0, true));
    vesnice->pridejNepritele(new Postava);

    cesta->setSeverniLokace(louka);
    cesta->setZapadniLokace(mesto);

    cesta->pridejPredmet(new Predmet("Hbite kalhoty", 13, 1, false));

    cesta->pridejNepritele(new Postava);

    hrbitov->setJizniLokace(les);

    louka->setJizniLokace(cesta);
    louka->setZapadniLokace(les);

    m_aktualniLokace = mesto;
    m_hrdina->setAktualniLokace(m_aktualniLokace);
}

void Hra::vyberAkci() {
    std::string rozhodnuti;

    std::cout << "Co chces delat?" << std::endl;
    std::cout << "Mas tyto moznosti: " << std::endl;
    std::cout << "Jit [j]inam, sebrat [p]redmet, [b]ojovat." << std::endl;
    std::cin >> rozhodnuti;
    if (rozhodnuti[0] == 'j') {
        jdiJinam();
//    } else if (rozhodnuti == 'p') {
//        std::cout << "Ktery predmet chces?" << std::endl;
//        for (int i = 0; i < m_predmety.size(); ++i) {
//            std::cout << "[" << i << "] " << std::endl;
//            m_predmety.at(i)->printInfo();
//        }
//        std::cin.get(rozhodnuti2);
//        //  Hrdina->pridejPredmet(m_predmety.at(std::stoi(&rozhodnuti2)));
//        m_predmety.erase(m_predmety.begin() + std::stoi(&rozhodnuti2));
//    } else if (rozhodnuti == 'b') {
//        std::cout << "Vyber nepritele." << std::endl;
//        for (int i = 0; i < m_nepratele.size(); ++i) {
//            std::cout << "[" << i << "] " << std::endl;
//            m_nepratele.at(i)->printInfo();
//        }
//        std::cin.get(rozhodnuti2);
//
//        //    while (m_nepratele.at(std::stoi(&rozhodnuti2))->getZdravi() > 0 and Hrdina->getZdravi() > 0) {
//        //    m_nepratele.at(std::stoi(&rozhodnuti2))->zautoc();
//        //    Hrndina->branSe();
//        //    Hrndina->Zautoc();
//        //    m_nepratele.at(std::stoi(&rozhodnuti2))->branSe();
//
//    }
    }
}
//
//void Hra::setOkolniLokace(Lokace *sever, Lokace *jih, Lokace *zapad, Lokace *vychod) {
//    m_aktualniLokace->setSeverniLokace(sever);
//    m_aktualniLokace->setJizniLokace(jih);
//    m_aktualniLokace->setZapadniLokace(zapad);
//    m_aktualniLokace->setVychodniLokace(vychod);
//}

void Hra::jdiJinam() {
    std::string rozhodnuti;

    std::cout << "Kam chces jit?" << std::endl;
    if (m_hrdina->getAktualniLokace()->getSeverniLokace() != nullptr) {
        std::cout << "Na [s]everu je " << m_hrdina->getAktualniLokace()->getSeverniLokace()->getNazev() << std::endl;
    }
    if (m_hrdina->getAktualniLokace()->getJizniLokace() != nullptr) {
        std::cout << "Na [j]ihu je " << m_hrdina->getAktualniLokace()->getJizniLokace()->getNazev() << std::endl;
    }
    if (m_hrdina->getAktualniLokace()->getZapadniLokace() != nullptr) {
        std::cout << "Na [z]apade je " << m_hrdina->getAktualniLokace()->getZapadniLokace()->getNazev() << std::endl;
    }
    if (m_hrdina->getAktualniLokace()->getVychodniLokace() != nullptr) {
        std::cout << "Na [v]ychode je " << m_hrdina->getAktualniLokace()->getVychodniLokace()->getNazev() << std::endl;
    }
    std::cin >> rozhodnuti;
    if (rozhodnuti[0] == 's') {
        m_hrdina->setAktualniLokace(m_hrdina->getAktualniLokace()->getSeverniLokace());
    } else if (rozhodnuti[0] == 'j') {
        m_hrdina->setAktualniLokace(m_hrdina->getAktualniLokace()->getJizniLokace());
    } else if (rozhodnuti[0] == 'z') {
        m_hrdina->setAktualniLokace(m_hrdina->getAktualniLokace()->getZapadniLokace());
    } else if (rozhodnuti[0] == 'v') {
        m_hrdina->setAktualniLokace(m_hrdina->getAktualniLokace()->getVychodniLokace());
    } else {
        std::cout << "Spatny vstup." << std::endl;
    }
}


void Hra::spustitHru() {
    char kon;

//    PostavaDirector* hrdinaDirector = new PostavaDirector(new HrdinaBuilder());
//    hrdinaDirector->setPostavaBuilder(new HrdinaBuilder());
//    Hrdina* hrdina = static_cast<Hrdina *>(hrdinaDirector->sestavPostavu());
//    //hrdina->printInfo();
//
//    PostavaDirector* nastvanySousedDirector = new PostavaDirector(new NastvanySousedBuilder());
//    nastvanySousedDirector->setPostavaBuilder(new NastvanySousedBuilder());
//    Postava* nastvanySoused = nastvanySousedDirector->sestavPostavu();
//    //nastvanySoused->printInfo();
//
//    PostavaDirector* banditaDirector = new PostavaDirector(new BanditaBuilder());
//    banditaDirector->setPostavaBuilder(new BanditaBuilder());
//    Postava* bandita = banditaDirector->sestavPostavu();
//    Postava* bandita2 = banditaDirector->sestavPostavu();
//    //bandita->printInfo();
//
//    LokaceDirector* poleDirector = new LokaceDirector(new PoleBuilder);
//    poleDirector->setLokaceBuilder(new PoleBuilder());
//    Lokace* pole = poleDirector->sestavLokaci();
//    pole->pridejNepritele(bandita2);
//    //pole->printInfo();
//
//    LokaceDirector* mestoDirector = new LokaceDirector(new MestoBuilder);
//    mestoDirector->setLokaceBuilder(new MestoBuilder());
//    Lokace* mesto = mestoDirector->sestavLokaci();
//    mesto->pridejNepritele(nastvanySoused);
//    //mesto->printInfo();
//
//    LokaceDirector* lesDirector = new LokaceDirector(new LesBuilder);
//    lesDirector->setLokaceBuilder(new LesBuilder());
//    Lokace* les = lesDirector->sestavLokaci();
//    les->pridejNepritele(bandita);
//    //les->printInfo();
//
//
//    std::string jmeno;
    std::cout << "Ahoj hraci!" << std::endl;
//    std::cout << "Jake je tvoje jmeno?" << std::endl;
//    std::cin >> jmeno;
//    m_hrdina->setJmeno(jmeno) ;
//    std::cout << std::endl;

    while (!m_konec) {
        std::cout << "Pokud budes chtit ukoncit hru stiskni: = " << std::endl;
        std::cout << std::endl;
        //std::cout << m_hrdina->getJmeno() << " se prave nachazi zde:" << std::endl;
        std::cout << "Prave se nachazis zde:" << std::endl;
        m_hrdina->getAktualniLokace()->printInfo();
        vyberAkci();

        kon = _getch();
        if (kon == 61) {
            std::cout << "Konec hry" << std::endl;
            m_konec = true;
        } else {
            continue;
        }
    };
}

//void Hra::seberPredmet() {
//    m_hrdina->seberPredmet()
//}

//kdyz aktualni lokace vypise neco z nasledujicich atd...

//        pole->printInfo();
//        pole->vyberAkci();
//
//        mesto->printInfo();
//        mesto->vyberAkci();
//
//        les->printInfo();
//        les->vyberAkci();


//dalsi vyber vypsat aktualni predmety???
//hrdina->vypisPredmety();

//popřípadě info o Hrdinovi..
//hrdina->printInfo();

