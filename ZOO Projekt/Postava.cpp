//
// Created by zajda on 07.05.2021.
// Updated by Majkl on 07.05.2021.
//

#include <iostream>
#include "Postava.h"

Postava::Postava() {
    m_jmeno = "Kolemjdouci";
    m_zdravi = 0;
    m_sila = 0;
    m_hbitost = 0;
    m_inteligence = 0;
}

void Postava::setZdravi(int zdravi) {
    m_zdravi = zdravi;
}

void Postava::setSila(int sila) {
    m_sila = sila;
}

void Postava::setInteligence(int inteligence) {
    m_inteligence = inteligence;
}

void Postava::setHbitost(int hbitost) {
    m_hbitost = hbitost;
}

void Postava::setJmeno(std::string jmeno) {
    m_jmeno = jmeno;
}

void Postava::seberPredmet(Predmet *predmet) {
    if (m_predmety.size() < 10) {
        m_predmety.push_back(predmet);
    } else {
        std::cout << "Víc toho neuneseš! Budeš muset něco zahodit." << std::endl;
    }
}

void Postava::pouzijPredmet(int pozice) {
    if (0 <= pozice && pozice < m_predmety.size()) {
        if (m_predmety.at(pozice)->getTyp() == 0) {
            m_zdravi += m_predmety.at(pozice)->getHodnota();

        } else if (m_predmety.at(pozice)->getTyp() == 1) {
            m_hbitost += m_predmety.at(pozice)->getHodnota();

        } else if (m_predmety.at(pozice)->getTyp() == 2) {
            m_sila += m_predmety.at(pozice)->getHodnota();

        } else if (m_predmety.at(pozice)->getTyp() == 3) {
            m_inteligence += m_predmety.at(pozice)->getHodnota();
        }

        else if (m_predmety.at(pozice)->getTyp() == 100) {
            std::cout << "Tento predmet pouzivas k utoku!" << std::endl;
        }
        else if (m_predmety.at(pozice)->getTyp() == 200) {
            std::cout << "Tento predmet pouzivas k obrane!" << std::endl;
        }

    } else {
        std::cout << "Spatne misto v inventari!" << std::endl;
    }

}

void Postava::zahodPredmet(int pozice) {
    if (0 <= pozice && pozice < m_predmety.size()) {
        m_predmety.erase(m_predmety.begin() + pozice);
        std::cout << "Predmet zahozen!" << std::endl;
    } else {
        std::cout << "Spatne misto v inventari!" << std::endl;
    }

}

int Postava::getUtok() {
    int u = m_sila;

    for (int i = 0; i < m_predmety.size(); ++i) {

        if (m_predmety.at(i)->getTyp() == 100) {

            u = u + m_predmety.at(i)->getHodnota();
        }
    }
    return u;
}

int Postava::getObrana() {
    int o = m_zdravi;

    for (int i = 0; i < m_predmety.size(); ++i) {

        if (m_predmety.at(i)->getTyp() == 200) {

            o = o + m_predmety.at(i)->getHodnota();
        }
    }
    return o;
}
/*
void Postava::zmenLokaci(Lokace *lokace) {

}

void Postava::setAktualniLokace(Lokace *lokace) {

}
*/
void Postava::printInfo() {
    std::cout << " Nazev postavy: " << m_jmeno << std::endl;
    std::cout << "   Zdravi: " << m_zdravi << std::endl;
    std::cout << "   Sila: " << m_sila << std::endl;
    std::cout << "   Hbitost: " << m_hbitost << std::endl;
    std::cout << "   Inteligence: " << m_inteligence << std::endl;
    std::cout << std::endl;
}

void Postava::zautoc() {


}

void Postava::branSe() {

}

void Postava::vypisPredmety() {
    for (int i = 0; i < m_predmety.size(); ++i) {
        std::cout << "-- Predmet cislo: " << i << " --" << std::endl;
        m_predmety.at(i)->printInfo();
        std::cout << std::endl;

    }

}

Postava::~Postava() {
    std::cout << "Postava zemrela!" << std::endl;

}

std::string Postava::getJmeno() {
    return m_jmeno;
}

